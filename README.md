# Git Down with Markdown

**THIS IS STARTER TEXT. ONCE FINISHED, DELETE THIS TEXT AND WRITE YOUR OWN INTRO!**

This is an example of a "book" of documentation made with two incredible technologies: Git and Markdown. Using these two together, organizations can rapidly and efficiently develop linked documentation that:

* Preserves institutional memory
* Maintains role accountability
* Clarifies policy/procedure

This repository is designed to work with [Gitbook](https://gitbook.com) to produce a website, ebook, or PDF of your documentation.

## Writing Instructions
This file, and [SUMMARY.md](SUMMARY.md) must be present. `README` is the front matter to your book. Delete this content and write your own.

`SUMMARY` is the book's Table of Contents. When you add new chapters (new files), you will also have to add them to `SUMMARY`.

Use good [Markdown formatting](http://commonmark.org/help/) to produce the best books possible.

Don't know Markdown yet? Try CommonMark's [tutorial](http://commonmark.org/help/tutorial/)

We recommend a Markdown-friendly text-editor like:

* [Visual Studio Code](https://code.visualstudio.com)
* [Atom](https://atom.io)
* [Typora](https://typora.io)
* [Haroopad](http://pad.haroopress.com/)

## Windows VS Code Instructions
This is a brief tutorial on how to get VS Code installed and running with Git.

### Installation

#### Installing Git on Windows
* Grab the installer for [Git on Windows](https://git-scm.com/downloads).
    * Run the installer.
    * For ease of install, just use all the default options when asked to choose the options during the install process.

##### Git Folder Location
* If default install location was selected, make note of the folder location at:
    * `C:\Program Files\Git\`

#### Installing VS Code on Windows
* Grab the installer from the [VS Code website](https://code.visualstudio.com/download).
    * Run the installer. By default, VS Code is installed under `C:\Program Files (x86)\Microsoft VS Code` for a 64-bit system.
        * **Note**: If using **Windows 7**, make sure that at least [.NET Framework 4.5.2](https://www.microsoft.com/en-us/download/details.aspx?id=42643) is installed(If using VSCode for more than documentation purposes).
        * **UPDATE**: Verified working on fresh Windows 7 Pro SP1 (Released Feb. 22, 2011) and .NET Framework 3.5.
        * **Note**: To check to see what version of Net Framework, do:
            * On `Start Menu`, select `Run`.
            * Type `regedit.exe` and run it
            * Traverse to folder location: `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP`

### Setup
When you run VS Code for the first time, the window should look something like this: ![vspicture](images/vscodestart.jpg)

If not, select the `Help` option on the top menu, then select `Welcome`. 

Once you confirm the screen above, do the following:

* Select the option `Clone Git Repository`
* Enter the `https:` address of your online repository ([Github](https://github.com/), [Bitbucket](https://bitbucket.org/), etc)

#### Setting Git commands for VS Code terminal
* Select `File` from the top menu and select `Preferences` ->  `Settings`
    * Opens the `settings.json` file. Need to add the following in between the {} *(Assuming default install from [Git Folder Location section](#git-folder-location).)*
        * `"terminal.integrated.shell.windows": "C:\\Program Files\\Git\\git-cmd.exe"`
* Restart VS Code Program
* Select `View` from the top menu and select `Integrated Termainal` (or ctrl+`)
* Select `git-cmd.exe` option from the drop down menu on the right side of the terminal.


## Gitbook Instructions
When your book is ready to go, head over to [gitbook.com](https://gitbook.com) and create an account. From there, you can follow instructions how how to import a GitHub repository to use GitBook's service to create a well-structured book of your documentation.